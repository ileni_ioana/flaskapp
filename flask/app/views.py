from app import app
from flask import render_template, request, redirect
from app.services import data_utils, xnow_client

ALLOWED_EXTENSIONS = {'npz'}
XNOW_MODEL_CLIENT = xnow_client.XNowClient('../tfserving/models/keras/xnow/model_20200120-155658.hdf5')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route("/")
def index():
    return render_template('index.html', timesteps_ahead=0, message='')


@app.route("/xnow", methods=["GET", "POST"])
def xnow():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'timestep' not in request.files:
            return render_template('index.html', timesteps_ahead=0, message='not_selected', prediction='')

        file = request.files['timestep']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            return render_template('index.html', timesteps_ahead=0, message='empty', prediction='')

        if file and allowed_file(file.filename):
            inputs = data_utils.get_input([file])
            prediction = XNOW_MODEL_CLIENT.predict(inputs)

            return render_template('index.html', timesteps_ahead=0, message='uploaded ' + file.filename,
                                   prediction=prediction)
        else:
            return render_template('index.html', timesteps_ahead=0, message='wrong extension', prediction='')

    return render_template('index.html', timesteps_ahead=1, message='', prediction='')
