import json
import sparse
import numpy as np
import tensorflow.keras.backend as K


mins = np.loadtxt('app/static/files/mins.csv', delimiter=',')
maxs = np.loadtxt('app/static/files/maxs.csv', delimiter=',')
CROP_SIZE = (400, 400)


def crop_sample(crop_size, sample):
    raw_start = int((sample.shape[1] - crop_size[0]) / 2)
    raw_end = raw_start + crop_size[0]
    col_start = int((sample.shape[2] - crop_size[1]) / 2)
    col_end = col_start + crop_size[1]
    crop = sample[:, raw_start:raw_end, col_start:col_end]

    return crop


def get_input(files):
    inputs = []

    for file in files:
        # load the input
        sample = np.array(sparse.load_npz(file).todense(), dtype=float)
        sample = crop_sample(CROP_SIZE, sample)
        norm_sample = np.array([1.0 * (sample[channel] - mins[channel]) / (maxs[channel] - mins[channel])
                                for channel in range(sample.shape[0])])

        inputs.append(norm_sample.tolist())

    return inputs


def get_request_json(files):
    inputs = get_input(files)

    json_input = json.dumps({'instances': inputs})

    return json_input


def root_mean_squared_error(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_pred - y_true)))




