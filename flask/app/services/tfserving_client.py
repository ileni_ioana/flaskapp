import numpy as np
import requests


# Define a Base client class for Tensorflow Serving
class TFServingClient:
    """
    This is a base class that implements a Tensorflow Serving client
    """
    TF_SERVING_URL_FORMAT = '{protocol}://{hostname}:{port}/v1/models/{model_name}:predict'

    def __init__(self, hostname, port, model_name, protocol="http"):
        self.protocol = protocol
        self.hostname = hostname
        self.port = port
        self.model_name = model_name

    def query_service(self, req_json):
        """
        :param req_json: dict (as define in https://cloud.google.com/ml-engine/docs/v1/predict-request)
        :return: dict
        """
        server_url = self.TF_SERVING_URL_FORMAT.format(protocol=self.protocol,
                                                       hostname=self.hostname,
                                                       port=self.port,
                                                       model_name=self.model_name)
        print('URL is: ', server_url)
        response = requests.post(server_url, json=req_json)
        response.raise_for_status()

        print('Full: ', response)

        return np.array(response.json()['predictions'])
