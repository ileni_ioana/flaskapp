import tensorflow.keras as keras
from app.services.data_utils import root_mean_squared_error


# Define a specific client for our xnow model
class XNowClient():
    # INPUT_NAME is the config value we used when saving the model (the only value in the `input_names` list)

    def __init__(self, model_path):
        keras.utils.get_custom_objects().update({"root_mean_squared_error": root_mean_squared_error})
        self.model = keras.models.load_model(model_path)

    def predict(self, inputs):
        prediction = self.model.predict(inputs)

        return prediction


