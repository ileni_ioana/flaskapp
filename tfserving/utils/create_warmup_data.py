import tensorflow as tf
import requests
import base64
from app.services.data_utils import get_request_json
from tensorflow.python.framework import tensor_util
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_log_pb2

NUM_RECORDS = 10


def main():
    with tf.io.TFRecordWriter("tf_serving_warmup_requests") as writer:
        proc_input = get_request_json(['tfserving/utils/20170603_002.npz'])
        predict_request = predict_pb2.PredictRequest()
        predict_request.model_spec.name = 'xnow'
        predict_request.model_spec.signature_name = 'serving_default'
        predict_request.inputs['input_6'].CopyFrom(
            tensor_util.make_tensor_proto([proc_input], tf.string))
        log = prediction_log_pb2.PredictionLog(
            predict_log=prediction_log_pb2.PredictLog(request=predict_request))
        for r in range(NUM_RECORDS):
            writer.write(log.SerializeToString())


if __name__ == "__main__":
    main()
