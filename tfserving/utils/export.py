import os
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import get_custom_objects
from tensorflow.python.framework.errors_impl import InvalidArgumentError

MODEL_EXPORT_DIR = 'models/tfserving'
MODEL_PATH = 'models/keras'


def root_mean_squared_error(y_true, y_pred):
  try:
    tf.debugging.check_numerics(y_pred, 'Y_PRED: inf or nan')
  except InvalidArgumentError as e:
    tf.print('Y_PRED EXCEPTION: ', e)
  except StopIteration:
    return

  try:
    tf.debugging.check_numerics(y_true, 'Y_TRUE: inf or nan')
  except InvalidArgumentError as e:
    tf.print('Y_TRUE EXCEPTION: ', e)
  except StopIteration:
    return

  return K.sqrt(K.mean(K.square(y_pred - y_true)))


def load_keras_model(relative_path):
    model_path = os.path.join(MODEL_PATH, relative_path)
    get_custom_objects().update({"root_mean_squared_error": root_mean_squared_error})

    model = load_model(model_path)

    return model, model_path


def export_model_tf_format(model, relative_path):
    export_path = os.path.join(MODEL_EXPORT_DIR, relative_path)

    os.makedirs(export_path, exist_ok=True)

    versions = sorted(os.listdir(export_path), reverse=True)

    if len(versions) > 0:
        version = str(int(versions[0]) + 1)
    else:
        version = '1'

    export_path = os.path.join(export_path, version)

    tf.saved_model.save(model, export_path)

    return export_path


def main():
    model, model_path = load_keras_model('xception/model_20200120-155658.hdf5')
    print('Loaded Keras model: ', model_path)

    export_path = export_model_tf_format(model, 'xception')
    print("Model exported to: ", export_path)


if __name__ == '__main__':
    main()
